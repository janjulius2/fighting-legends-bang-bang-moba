﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public class Manager : NetworkManager
    {
        public Manager(bool client)
        {
            this.StartServer();
        }

        public override void OnServerConnect(NetworkConnection conn)
        {
            Debug.Log("player connected" + conn.address);
        }

    }
}
