﻿using Assets.Scripts.Champion;
using Assets.Scripts.Champion.Champions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public int pick;

    public Text test;

    public Champion myChampion;
    public Canvas personalInterface;

    public Canvas myInterface;

    private int team = 0;

    public void Start()
    {
        print(myInterface);
        transform.name = "player " + GetNetworkId();
        myChampion = addChampionClass(Random.Range(0, 2));
        test = FindObjectOfType<Text>();
        myInterface = Instantiate(personalInterface);
        myInterface.name = "Interface of player " + GetNetworkId();
        myInterface.GetComponent<PlayerInterface>().setId(GetNetworkId());
    }

    public void FixedUpdate()
    {
        print(myInterface);
        print(myChampion);
        if (myInterface != null)
        {
            myInterface.GetComponent<PlayerInterface>().UpdateInterface(myChampion.getName(), myChampion.currentHealth);
        }

        if (Input.GetKey(KeyCode.I))
        {
            myChampion.setHealth(myChampion.getHealth() - 100);
        }
    }

    public NetworkInstanceId GetNetworkId()
    {
        return GetComponent<NetworkIdentity>().netId;
    }

    public Champion addChampionClass(int id)
    {
        print("Picking champoin of id" + id);
        switch (id)
        {
            case 0:
                return gameObject.AddComponent<Rocky>();
            case 1:
                return gameObject.AddComponent<Berend>();
        }
        return null;
    }
}
