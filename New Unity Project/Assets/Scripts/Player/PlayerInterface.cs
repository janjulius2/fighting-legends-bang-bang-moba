﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Champion;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerInterface : MonoBehaviour
{
    public Text[] textComponents;

    private bool firstLoad = true;
    private NetworkInstanceId myPlayerNetworkId;

    public void setId(NetworkInstanceId netId)
    {
        myPlayerNetworkId = netId;
    }

    public void UpdateInterface(string name, float h)
    {
        if (firstLoad)
        {
            textComponents[0].text = name;

        //    firstLoad = false;
        }
        textComponents[1].text = h.ToString();
    }
}
