﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts
{
    public class Main : MonoBehaviour
    {
        private GameObject[] charObjects = new GameObject[100];

        void Start()
        {
            charObjects[0] = (GameObject) Resources.Load("Prefabs/Champions/Raccoon/raccoon", typeof(GameObject));
            charObjects[1] = (GameObject)Resources.Load("Prefabs/Champions/Yeti/yeti", typeof(GameObject));
        }

        public GameObject getCharacterModel(int id)
        {
            return charObjects[id];
        }
    }
}
