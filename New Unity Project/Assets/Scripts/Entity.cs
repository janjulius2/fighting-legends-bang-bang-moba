﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Entity : NetworkBehaviour {

    private float baseHealth,
        baseHealthIncrease,
        baseHealthRegen,
        baseMana,
        baseManaIncrease,
        baseManaRegen,
        baseRange,
        baseAd,
        baseAdIncrease,
        baseAp,
        baseApIncrease,
        baseAttackSpeed,
        baseAttackSpeedIncrease,
        baseArmor,
        baseArmorIncrease,
        baseMr,
        baseMrIncrease,
        baseMoveSpeed,
        baseMoveSpeedIncrease,
        baseCritChance,
        baseCritDamage;

    private float itemArmor,
        itemMagicResist,
        itemHealth,
        itemMana,
        itemHealthRegen,
        itemManaRegen,
        itemDamage,
        itemAbilityPower,
        itemLifeSteal,
        itemAttackSpeed,
        itemCritChance,
        itemCoolDownReduction, 
        itemMovementSpeed;

    public CrowdControlStatus ccStatus;
    private float ccTimer;

    public GameObject model;

    [SyncVar]
    public float currentHealth = 0;

    [SyncVar]
    private float currentMana;


    /// <summary>
    /// wether the entity should regenerate its health
    /// </summary>
    private bool regenHealth = false;

    /// <summary>
    /// wether the entity has mana
    /// </summary>
    private bool haveMana = false;

    public void StunEntity(float duration)
    {
        ccStatus = CrowdControlStatus.Stun;
    }

    public void SlowEntity(float duration, int slowPercentage)
    {
        ccStatus = CrowdControlStatus.Slow;
    }

    public void SilenceEntity(float duration)
    {
        ccStatus = CrowdControlStatus.Silence;
    }

    public void RootEntity(float duration)
    {
        ccStatus = CrowdControlStatus.Root;
    }

    public void FixedUpdate()
    {
        #region Crowd Control checking and timers
        if(ccStatus != CrowdControlStatus.Normal)
        {
            ccTimer -= Time.deltaTime;
            if(ccTimer <= 0)
            {
                ccStatus = CrowdControlStatus.Normal;
            }
        }

        #endregion
        

    }
}
