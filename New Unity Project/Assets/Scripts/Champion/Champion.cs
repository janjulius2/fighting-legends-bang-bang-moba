﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace Assets.Scripts.Champion
{
    public class Champion : Entity
    {
        public string Name, BeforeName, AfterName;
        public string modelString;

        public int cid;
        private Player myPlayer;
        private PlayerInterface myInterface;
            
        /// <summary>
        /// character base stats
        /// </summary>
        private int kills, deaths, assist, gold, level;

        private bool firstLoad = true;
        /// <summary>
        /// Cooldowns of abilities indexing from 0 to 4
        /// 0 = passive
        /// 1 = q, 2 = w, 3 = e, 4 = r
        /// </summary>
        private float[] coolDownAbility;

        private int[] abilityLevel;

        public void Start()
        {
            myPlayer = GetComponent<Player>();
            myInterface = GetComponent<PlayerInterface>();
        }

        public void Update()
        {
            //UpdateInterface();
        }

        public bool CastAbility(int ability)
        {
            if (coolDownAbility[ability] <= 0)
            {
                return true;
            }
            return false;
        }

        public void printName()
        {
            print(BeforeName + " " + Name + " " + AfterName);
        }

        public string getName()
        {
            if (BeforeName != "")
            {
                return BeforeName + " " + Name + " " + AfterName;
            }
            else
            {
                return Name + " " + AfterName;
            }
        }

        public void setHealth(float val)
        {
            currentHealth = val;
        }

        public float getHealth()
        {
            return currentHealth;
        }


        public void LoadInfo()
        {
            cid = Constants.getCharIdByName(Name);
        }

        #region override methods
        public void SpawnModel(GameObject caller)
        {
            GameObject myModel = Instantiate(FindObjectOfType<Main>().getCharacterModel(cid).gameObject);
            myModel.transform.parent = caller.transform;
        }
#endregion
    }
}
