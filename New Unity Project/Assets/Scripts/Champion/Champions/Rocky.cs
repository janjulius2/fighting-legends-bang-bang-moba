﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.Champion.Champions
{
    public class Rocky : Champion
    {
        public void Start()
        {
            Name = "Rocky";
            AfterName = "The Raccoon";
            LoadInfo();
            SpawnModel(gameObject);
        }

    }
}
