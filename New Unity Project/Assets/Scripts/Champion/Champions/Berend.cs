﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.Champion.Champions
{
    public class Berend : Champion
    {
        public void Start()
        {
            Name = "Berend";
            AfterName = "the Yeti";
            LoadInfo();
            SpawnModel(gameObject);
        }

    }
}
