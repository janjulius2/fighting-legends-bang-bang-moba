﻿public enum CrowdControlStatus
{
    Normal,
    Silence,
    Slow,
    Root,
    Stun
}
